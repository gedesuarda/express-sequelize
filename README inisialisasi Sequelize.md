**Sequelize Initialisasi project.**

_Sequelize memerlukan NodeJS, Express dan database untuk bisa berjalan, di step ini diasumsikan semua sudah terinstal dgn database username dan password yg akan diperlukan  di step selanjutnya._
_Ketik perintah ini di folder project_
```
> npm init -y
> npm install pg sequelize sequelize-cli
> npx sequelize-cli init
> npm install -D nodemon

```


_Edit file config/config.json di folder project . Ganti username, password, dialect dan nama database sesuaikan dengan data yg akan dipakai. dalam contoh ini sistem database yang digunakan adalah postgreSQL maka di tulis "dialect" : postgres" sesuaikan dgn database yg akan digunakan_
```
{
  "development": {
    "username": "username",
    "password": "password",
    "database": "database_development",
    "host": "127.0.0.1",
    "dialect": "postgres"
  },
  "test": {
    "username": "username",
    "password": "password",
    "database": "database_test",
    "host": "127.0.0.1",
    "dialect": "postgres"
  },
  "production": {
    "username": "username",
    "password": "password",
    "database": "database_production",
    "host": "127.0.0.1",
    "dialect": "postgres"
  }
}
```

_kemudian jalankan perintah ini untuk membuat database_

`> npx sequelize-cli db:create`


_Membuat model ,
 note : "Book" adalah nama table yang akan dibuat, sesuaikan dgn kebutuhan
        "title", "author" adalah nama kolom dalam tabel, sesuaikan dengan kebutuhan  ._

`> npx sequelize-cli model:generate --name Book --attributes title:string,author:string`


_Menjalankan migrasi._

`> npx sequelize-cli db:migrate`


_Ketik perintah ini untuk mejalankan aplikasi_

`> node index.js`

_semua logika dan fungsi dari aplikasi/apps ditulis di file index.js, silahkan di edit sesuai kebutuhan._
